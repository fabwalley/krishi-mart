package com.fabwalley.krishimart.utils

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson

object AppPreferences {
    const val dollor = "$"
    const val BASEURL = "http://3.7.224.122/talli/"
    private const val NAME = "tallicustomer123"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    private const val IsLogin = "IsLoginPref"
    private const val vertical = "vertical"
    private const val JwtToken = "token"
    private const val addkey = "add"
    private const val UserData = "UserData"
    private const val FcmToken = "PushToken"

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    private inline fun SharedPreferences.edit(operation:
                                                  (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)

        editor.apply()
    }

/*
   var SharedPreferences.clearValues
        get() = { }
        set(value) {
            edit {
                it.clear()
            }
        }
*/

    fun clearPref() {
        val editor = preferences.edit();
        editor.remove(IsLogin)
        editor.remove(JwtToken)
        editor.remove(UserData)
        editor.remove(addkey)
        editor.clear()
        editor.apply()
//        preferences.edit().clear().apply()
    }

    var isLogin: Boolean
        get() = preferences.getBoolean(IsLogin, false)
        set(value) = preferences.edit { it.putBoolean(IsLogin, value) }

    var isVertical: Boolean
        get() = preferences.getBoolean(vertical, false)
        set(value) = preferences.edit { it.putBoolean(vertical, value) }

    var token: String?
        get() = preferences.getString(JwtToken, "")
        set(value) = preferences.edit { it.putString(JwtToken, value) }

//    var userPreferance: LoginResponse?
//        get() = Gson().fromJson(preferences.getString(UserData, ""), LoginResponse::class.java)
//        set(value) = preferences.edit {
//            it.putString(UserData, Gson().toJson(value))
//        }
//
//    var address: Birthdaymodel?
//        get() = Gson().fromJson(preferences.getString(addkey, ""), Birthdaymodel::class.java)
//        set(value) = preferences.edit {
//            it.putString(addkey, Gson().toJson(value))
//        }

    var pushToken: String?
        get() = preferences.getString(JwtToken, "")
        set(value) = preferences.edit {
            it.putString(JwtToken, value)
        }


}