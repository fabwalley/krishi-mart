package com.fabwalley.krishimart.fragment

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fabwalley.krishimart.R
import com.fabwalley.krishimart.activity.BrandActivity
import com.fabwalley.krishimart.adapter.BrandAdapter
import com.fabwalley.krishimart.adapter.FeaturedAdapter
import com.fabwalley.krishimart.adapter.SearchAdapter
import com.fabwalley.krishimart.webservices.responsebean.HomeResponse
import com.talli.customer.common.BaseFragment
import com.talli.customer.listners.OnItemClickListner
import com.talli.customer.webservice.ApiClient
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.recyle_lay.*
import java.util.*

/**
 * Created By Rahul Kansara (Rk) \n Email : rahul.kansara10@gmail.com on 09-Aug-20.
 */


class SearchFragment : BaseFragment() {

    lateinit var adapter: SearchAdapter

    val DELAY_MS: Long = 2000 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 2000 // time in milliseconds between successive task executions.
    var timer: Timer? = null
    private var handler: Handler? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }


    companion object {
        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = SearchAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {
            }
        })
        recyleview.adapter = adapter
        apicall()

    }



    private fun apicall() {
        callApiAndShowDialog(call = {
            ApiClient.getClient(context!!.applicationContext as Application).getHome("homepageapi")
        }, handleSuccess = {
            adapter.addData(it.popular)
        }, handleGenric = {
            makeToast(it)
        },showDialg = false
        )
    }
}
