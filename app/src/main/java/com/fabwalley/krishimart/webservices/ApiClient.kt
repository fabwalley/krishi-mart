package com.talli.customer.webservice

import android.app.Application
import com.fabwalley.krishimart.webservices.ApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit

object ApiClient {

    private val readTimeOut: Long = 50
    private val connectionTimeOut: Long = 50

    val BASE_URL = "https://www.krishimart.in/"


    fun getClient(context: Application): ApiService {


        val okHttpClient = OkHttpClient.Builder().run {
            addInterceptor(object : Interceptor {
                @Throws(IOException::class)
                override fun intercept(chain: Interceptor.Chain): Response {
//                    val token: String? = AppPreferences.token
                    val requestBuilder = chain.request().newBuilder()
//                    token?.let {
//                        requestBuilder.addHeader("Authorization", "Bearer $it")
//                    }
//                    requestBuilder.addHeader("Content-Type", "application/json")
                    return chain.proceed(requestBuilder.build())
                }
            })
            connectTimeout(connectionTimeOut, TimeUnit.SECONDS)
            readTimeout(readTimeOut, TimeUnit.SECONDS)
            build()
        }

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .build()
            .create(ApiService::class.java)
    }


}