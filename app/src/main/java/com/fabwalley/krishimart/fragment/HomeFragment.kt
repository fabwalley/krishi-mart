package com.fabwalley.krishimart.fragment

import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.fabwalley.krishimart.R
import com.fabwalley.krishimart.activity.BrandActivity
import com.fabwalley.krishimart.activity.ViewAllActivity
import com.fabwalley.krishimart.adapter.*
import com.fabwalley.krishimart.webservices.responsebean.HomeResponse
import com.talli.customer.common.BaseFragment
import com.talli.customer.listners.OnItemClickListner
import com.talli.customer.webservice.ApiClient
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import kotlin.collections.ArrayList

class HomeFragment : BaseFragment() {

    lateinit var adapter: BannerAdapter
    lateinit var brandAdapter: BrandAdapter
    lateinit var popadapter: PopularAdapter
    lateinit var featureadapter: FeaturedAdapter
    lateinit var recentadapter: RecentAdapter
    lateinit var cateadapter: CateAdapter
    lateinit var response: HomeResponse
    val DELAY_MS: Long = 2000 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 2000 // time in milliseconds between successive task executions.
    var timer: Timer? = null
    private var handler: Handler? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }


    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        response = HomeResponse()

//        seeAllFeatured.underline()
////        seeAllBrands.underline()
//        seeAllPopular.underline()
////        seeAllCate.underline()
//        seeAllRecents.underline()

        seeAllPopular.setOnClickListener {
            startActivity(Intent(activity, ViewAllActivity::class.java).putExtra("data", response)
                .putExtra("which", getString(R.string.popular_products)))
        }

        seeAllFeatured.setOnClickListener {
            startActivity(
                Intent(activity, ViewAllActivity::class.java).putExtra("data", response)
                    .putExtra("which", getString(R.string.featured_products))
            )
        }
        seeAllRecents.setOnClickListener {
            startActivity(
                Intent(activity, ViewAllActivity::class.java).putExtra("data", response)
                    .putExtra("which", getString(R.string.recent_products))
            )
        }

        adapter = BannerAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {

            }
        })

        popadapter = PopularAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {

            }
        })
        featureadapter = FeaturedAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {

            }
        })

        brandAdapter = BrandAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {
                val data = obj as HomeResponse.Brand
                startActivity(
                    Intent(activity, BrandActivity::class.java).putExtra("data", response)
                        .putExtra("which", position)
                )
            }
        })
        recentadapter = RecentAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {

            }
        })
        cateadapter = CateAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {

            }
        })

        popularRv.adapter = popadapter
        recentRv.adapter = recentadapter
        banerRv.adapter = adapter;
        featuredRv.adapter = featureadapter;
        cateRv.adapter = cateadapter;
        banerRv.removeAllViews()
        banerRv.setCurrentItem(0)
        setBannerData();
        brandRv.adapter = brandAdapter
        apicall()
    }

    private fun apicall() {
        callApiAndShowDialog(call = {
            ApiClient.getClient(context!!.applicationContext as Application).getHome("homepageapi")
        }, handleSuccess = {
            response = it
            recentadapter.addData(it.resent)
            popadapter.addData(it.popular)
            brandAdapter.addData(it.brand)
            featureadapter.addData(it.featured)
            cateadapter.addData(it.category)
            hideLay.visibility = View.GONE
        }, handleGenric = {
            makeToast(it)
        }
        )
    }

    private fun setBannerData() {
        val data: ArrayList<Int> = ArrayList()
        data.add(R.drawable.banner1)
        data.add(R.drawable.banner2)
        data.add(R.drawable.banner3)
        adapter.addData(data)

        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                automateViewPagerSwiping()
            }
        }, PERIOD_MS)

    }


    private fun automateViewPagerSwiping() {
        if (handler == null) {
            handler = Handler()
        }
        val update = Runnable {
            if (context != null) {
                if (banerRv != null && adapter != null) {
                    if (banerRv.getCurrentItem() == adapter.count - 1) {
                        banerRv.setCurrentItem(0)
                    } else {
                        banerRv.setCurrentItem(banerRv.getCurrentItem() + 1, true)
                    }
                }
            }
        }
        if (timer == null) {
            timer = Timer()
        }
        timer!!.schedule(
            object : TimerTask() {
                // task to be scheduled
                override fun run() {
                    handler!!.post(update)
                }
            }, DELAY_MS, PERIOD_MS
        )
    }


    override fun onDetach() {
        timer!!.cancel()
        timer = null
        if (handler != null) {
            handler!!.removeCallbacksAndMessages(null)
        }
        super.onDetach()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (timer == null) {
            timer = Timer()
        }
    }

    fun TextView.underline() {
        paintFlags = paintFlags or Paint.UNDERLINE_TEXT_FLAG
    }
}
