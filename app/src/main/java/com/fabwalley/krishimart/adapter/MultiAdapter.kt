package com.fabwalley.krishimart.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.fabwalley.krishimart.R
import com.fabwalley.krishimart.webservices.responsebean.HomeResponse
import com.talli.customer.listners.OnItemClickListner
import kotlinx.android.synthetic.main.item_brand_hori.view.*
import kotlinx.android.synthetic.main.item_brand_hori.view.price
import kotlinx.android.synthetic.main.item_brand_hori.view.title
import kotlinx.android.synthetic.main.item_popular.view.*
import kotlinx.android.synthetic.main.pro_img.view.*
import kotlin.collections.ArrayList

class MultiAdapter(val activity: Context?, val itemClick: OnItemClickListner) :
    RecyclerView.Adapter<MultiAdapter.BannerViewHolder>() {
    class BannerViewHolder(view: View) : RecyclerView.ViewHolder(view)

    companion object {
        var selected = -1
    }

    var dataList: ArrayList<HomeResponse.Popular> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerViewHolder {
        return BannerViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.item_brand_hori, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: BannerViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        holder.itemView.title.setText(dataList[position].title)
        loadImage(dataList[position].image, holder.itemView.img, holder.itemView.pro)
        holder.itemView.des.setText("Kemcron Plus: Profenophos 40% + Cypermethrin 4% EC It is a combination product of organophosphorous and pyrethroids compound. It is a broad spectrum insecticide having contact and stomach action and is used to control bollworm on cotton.Packing Size: 250 ml, 500 ml")

//        if (position ==0){
//            holder.itemView.price.setText("₹ 100")
//        }else{
//            val a1 = dataList[position].price.replace("&#8377", "\u20B9")
//                .replace("&ndash", " - ")
//                .replace(";", "")
//
//            holder.itemView.price.setText(a1)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.itemView.price.setText(Html.fromHtml(dataList[position].price, Html.FROM_HTML_MODE_COMPACT));
        } else {
            holder.itemView.price.setText(Html.fromHtml(dataList[position].price));
        }
//        }


        holder.itemView.setOnClickListener {
            itemClick.onItemClick(dataList[position], position, position)
        }
    }


    fun loadImage(imageUrl: String?, imageView: ImageView?, progressBar: ProgressBar?) {
        if (imageView == null) {
            return
        }
        if (imageUrl == null) {
            imageView.setImageResource(R.drawable.no_image)
            if (progressBar != null) progressBar.visibility = View.GONE
            return
        }
        Glide.with(activity!!).load(imageUrl).listener(object : RequestListener<Drawable?> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any,
                target: Target<Drawable?>,
                isFirstResource: Boolean
            ): Boolean {
                if (progressBar != null) {
                    progressBar.visibility = View.GONE
                }
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any,
                target: Target<Drawable?>,
                dataSource: DataSource,
                isFirstResource: Boolean
            ): Boolean {
                if (progressBar != null) progressBar.visibility = View.GONE
                return false
            }
        }).placeholder(R.drawable.no_image)
            .into(imageView)
    }

    fun addData(brand: ArrayList<HomeResponse.Popular>) {
        dataList = brand;
        notifyDataSetChanged()
    }

    fun setClicked(intExtra: Int) {
        selected = intExtra
        notifyDataSetChanged()
    }
}