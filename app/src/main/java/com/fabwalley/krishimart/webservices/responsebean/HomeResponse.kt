package com.fabwalley.krishimart.webservices.responsebean

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


/**
 * Created By Rahul Kansara (Rk) \n Email : rahul.kansara10@gmail.com on 02-Aug-20.
 */

class HomeResponse : Serializable {
    @SerializedName("brand")
    lateinit var brand: ArrayList<Brand>

    @SerializedName("category")
    lateinit var  category: ArrayList<Category>

    @SerializedName("featured")
    lateinit var  featured: ArrayList<Featured>

    @SerializedName("popular")
    lateinit var popular: ArrayList<Popular>

    @SerializedName("resent")
    lateinit var  resent: ArrayList<Resent>

    @SerializedName("sale")
    lateinit var  sale: ArrayList<Sale>


    
    
     class Brand(
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("link")
        val link: String,
        @SerializedName("name")
        val name: String
    ) : Serializable

    
    
     class Category(
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("link")
        val link: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("subcategory")
        val subcategory: ArrayList<Subcategory>
    ) : Serializable

    
    
    class Featured(
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("link")
        val link: String,
        @SerializedName("price")
        val price: String,
        @SerializedName("short_description")
        val shortDescription: String,
        @SerializedName("title")
        val title: String
    ) : Serializable

    
    
    class Popular(
        val id: Int = 0,
        val image: String= "",
        val link: String="",
        val price: String="",
        val shortDescription: String="",
        val title: String=""
    ) : Serializable

    
    
    class Resent(
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("link")
        val link: String,
        @SerializedName("price")
        val price: String,
        @SerializedName("short_description")
        val shortDescription: String,
        @SerializedName("title")
        val title: String
    ) : Serializable

    
    
     class Sale(
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: String,
        @SerializedName("link")
        val link: String,
        @SerializedName("price")
        val price: String,
        @SerializedName("short_description")
        val shortDescription: String,
        @SerializedName("title")
        val title: String
    ) : Serializable

    
    
    class Subcategory(
        @SerializedName("id")
        val id: Int,
        @SerializedName("image")
        val image: Boolean,
        @SerializedName("link")
        val link: String,
        @SerializedName("name")
        val name: String
    ) : Serializable
}