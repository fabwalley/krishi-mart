package com.fabwalley.krishimart.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.fabwalley.krishimart.R
import com.talli.customer.listners.OnItemClickListner
import kotlinx.android.synthetic.main.item_banner.view.*

class BannerAdapter(val mContext: Context?, val listner: OnItemClickListner) : PagerAdapter() {

    var onBoardItems : ArrayList<Int> = ArrayList();


    override fun instantiateItem(container: ViewGroup, position: Int): View {
        val itemView: View = LayoutInflater.from(mContext).inflate(R.layout.item_banner, container, false)
        Glide.with(mContext!!)
            .load(onBoardItems[position])
            .centerCrop()
            .into(itemView.img)
        itemView.setOnClickListener {
            listner.onItemClick(onBoardItems[position], position, position)
        }
        container.addView(itemView)

        return itemView
    }
    override fun getCount(): Int {
        return onBoardItems.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view ==`object`
    }




    fun addData(category: ArrayList<Int>) {
        if (onBoardItems.size > 0) {
            onBoardItems.clear()
        }
        onBoardItems = category;
        notifyDataSetChanged()
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as CardView?)
    }


}