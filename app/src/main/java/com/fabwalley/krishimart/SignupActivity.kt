package com.fabwalley.krishimart

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.fabwalley.krishimart.R
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_signup.*

class SignupActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        back.setOnClickListener {
         onBackPressed()
        }

    }
}