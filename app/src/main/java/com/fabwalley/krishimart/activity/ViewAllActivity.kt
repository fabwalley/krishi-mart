package com.fabwalley.krishimart.activity

import android.graphics.drawable.GradientDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.fabwalley.krishimart.R
import com.fabwalley.krishimart.adapter.*
import com.fabwalley.krishimart.webservices.responsebean.HomeResponse
import com.talli.customer.common.NavigationActivity
import com.talli.customer.listners.OnItemClickListner
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.recyle_lay.*

class ViewAllActivity : NavigationActivity() {

    lateinit var data: HomeResponse

    lateinit var adapter: BannerAdapter
    lateinit var brandAdapter: BrandAdapter
    lateinit var popadapter: PopularAdapter
    lateinit var featureadapter: FeaturedAdapter
    lateinit var recentadapter: RecentAdapter
    lateinit var cateadapter: CateAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_all)
        data = HomeResponse()

        data = intent.getSerializableExtra("data") as HomeResponse
        headerTxt.text = intent.getStringExtra("which")
        back.setOnClickListener {
            onBackPressed()
        }

        recyleview.layoutManager = GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false)

        if (intent.getStringExtra("which").equals(getString(R.string.popular_products))) {
            popadapter = PopularAdapter(mContext, object : OnItemClickListner {
                override fun onItemClick(obj: Any, position: Int, task: Int) {

                }
            })
            popadapter.addData(data.popular)
            recyleview.adapter = popadapter
        } else if (intent.getStringExtra("which").equals(getString(R.string.featured_products))) {
            featureadapter = FeaturedAdapter(mContext, object : OnItemClickListner {
                override fun onItemClick(obj: Any, position: Int, task: Int) {

                }
            })
            featureadapter.addData(data.featured)
            recyleview.adapter = featureadapter
        } else if (intent.getStringExtra("which").equals(getString(R.string.recent_products))) {
            recentadapter = RecentAdapter(mContext, object : OnItemClickListner {
                override fun onItemClick(obj: Any, position: Int, task: Int) {

                }
            })
            recentadapter.addData(data.resent)
            recyleview.adapter = recentadapter
        }


    }
}