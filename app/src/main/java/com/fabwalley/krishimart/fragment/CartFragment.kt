package com.fabwalley.krishimart.fragment

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fabwalley.krishimart.R
import com.talli.customer.common.BaseFragment
import java.util.*

class CartFragment: BaseFragment(){


    val DELAY_MS: Long = 2000 //delay in milliseconds before task is to be executed
    val PERIOD_MS: Long = 2000 // time in milliseconds between successive task executions.
    var timer: Timer? = null
    private var handler: Handler? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cart, container, false)
    }



    companion object {
        fun newInstance(): CartFragment {
            return CartFragment()
        }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


}
