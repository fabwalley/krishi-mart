package com.fabwalley.krishimart.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.fabwalley.krishimart.base.ViewPagerAdapter
import com.fabwalley.krishimart.R
import com.fabwalley.krishimart.fragment.CartFragment
import com.fabwalley.krishimart.fragment.HomeFragment
import com.fabwalley.krishimart.fragment.SearchFragment
import com.fabwalley.krishimart.fragment.UserFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.talli.customer.common.BaseFragment
import com.talli.customer.common.NavigationActivity
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*


class HomeActivity() : NavigationActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    private var viewPagerAdapter: ViewPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initUI()
    }

    private fun initUI() {
        setUpViewPager()
    }

    private fun setUpViewPager() {



        viewPager.adapter = ViewPagerAdapter(supportFragmentManager, getFragmentList())
        bottomNavigation.setOnNavigationItemSelectedListener(this)
        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                bottomNavigation.menu.getItem(position).isChecked = true
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
//        viewPager.adapter = ViewPagerAdapter(this, getFragmentList()).also { viewPagerAdapter = it }
//        viewPager.offscreenPageLimit = viewPagerAdapter!!.itemCount

//        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
//            override fun onPageSelected(position: Int) {
//                super.onPageSelected(position)
//                KeyboardUtil.hideKeyboard(this@HomeActivity)
//                bottomNavigation.animate().translationY(0f)
//                bottomNavigation.menu.getItem(position).isChecked = true
//                updateToolBar(position)
//            }
//        })
        updateToolBar(viewPager.currentItem)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                selectViewpagerFragment(0)
                updateToolBar(0)
            }
            R.id.nav_search -> {
                selectViewpagerFragment(1)
                updateToolBar(1)
            }
            R.id.nav_cart -> {
                selectViewpagerFragment(2)
                updateToolBar(2)
            }
            R.id.nav_profile -> {
                selectViewpagerFragment(3)
                updateToolBar(3)
            }
        }
        return false
    }

    private fun getFragmentList(): List<BaseFragment>? {
        val fragments: MutableList<BaseFragment> = ArrayList()
        fragments.add(HomeFragment.newInstance())
        fragments.add(SearchFragment.newInstance())
        fragments.add(CartFragment.newInstance())
        fragments.add(UserFragment.newInstance())
        return fragments
    }

    private fun updateToolBar(position: Int) {
        when (position) {
            0 -> {
//                updateTitle(R.string.home)
            }
            1 -> {
//                updateTitle(R.string.search)
            }
            2 -> {
//                updateTitle(R.string.my_favourites)
            }
            3 -> {
//                updateTitle(R.string.my_account)
            }
        }
    }

    private fun selectViewpagerFragment(position: Int) {
        viewPager.currentItem = position
    }

    override fun onBackPressed() {
        if (viewPager.currentItem != 0) {
            viewPager.currentItem = 0
            return
        }
        super.onBackPressed()
    }

}
