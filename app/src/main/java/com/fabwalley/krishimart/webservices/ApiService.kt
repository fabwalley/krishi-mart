package com.fabwalley.krishimart.webservices

import com.fabwalley.krishimart.webservices.responsebean.BrandListResponse
import com.fabwalley.krishimart.webservices.responsebean.HomeResponse
import retrofit2.http.*


interface ApiService {

    @FormUrlEncoded
    @POST("woocommerce-api")
    suspend fun getHome(
        @Field("apitype") apitype: String
    ): HomeResponse

    @FormUrlEncoded
    @POST("woocommerce-api")
    suspend fun getByBrandId(
        @Field("apitype") apiType: String,
        @Field("brandid") brandid: Int
    ): BrandListResponse


}

enum class ReviewableType {
    B, // Barbershop
    H, //Hairstylist
    O, // Ownshop
}