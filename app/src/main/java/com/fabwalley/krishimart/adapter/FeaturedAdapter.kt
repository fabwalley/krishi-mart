package com.fabwalley.krishimart.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.fabwalley.krishimart.R
import com.fabwalley.krishimart.webservices.responsebean.HomeResponse
import com.talli.customer.listners.OnItemClickListner
import kotlinx.android.synthetic.main.item_popular.view.*
import kotlinx.android.synthetic.main.pro_img.view.*
import java.util.ArrayList

class FeaturedAdapter(val activity: Context?, val itemClick: OnItemClickListner) :
    RecyclerView.Adapter<FeaturedAdapter.BannerViewHolder>() {
    class BannerViewHolder(view: View) : RecyclerView.ViewHolder(view)

    companion object {
        var width = 1
    }

    var dataList: ArrayList<HomeResponse.Featured> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BannerViewHolder {
        return BannerViewHolder(
            LayoutInflater.from(activity).inflate(R.layout.item_popular, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: BannerViewHolder, position: Int) {

        holder.itemView.title.setText(dataList[position].title)
        loadImage(dataList[position].image,holder.itemView.imgPop,holder.itemView.pro)
        val a1 = dataList[position].price.replace("&#8377;"," ₹")
            .replace("&ndash;"," - ")
        holder.itemView.price.setText(a1)


        holder.itemView.setOnClickListener {
            itemClick.onItemClick(dataList[position], position, position)
        }
    }

    fun addData(popular: ArrayList<HomeResponse.Featured>) {
        dataList = popular;
        notifyDataSetChanged()
    }

    fun setWidth(i: Int) {
        width = i;
    }


    fun loadImage(imageUrl: String?, imageView: ImageView?, progressBar: ProgressBar?) {
        if (imageView == null) {
            return
        }
        if (imageUrl == null) {
            imageView.setImageResource(R.drawable.no_image)
            if (progressBar != null) progressBar.visibility = View.GONE
            return
        }
        Glide.with(activity!!).load(imageUrl).listener(object : RequestListener<Drawable?> {
            override fun onLoadFailed(
                e: GlideException?,
                model: Any,
                target: Target<Drawable?>,
                isFirstResource: Boolean
            ): Boolean {
                if (progressBar != null) {
                    progressBar.visibility = View.GONE
                }
                return false
            }

            override fun onResourceReady(
                resource: Drawable?,
                model: Any,
                target: Target<Drawable?>,
                dataSource: DataSource,
                isFirstResource: Boolean
            ): Boolean {
                if (progressBar != null) progressBar.visibility = View.GONE
                return false
            }
        }).placeholder(R.drawable.no_image)
            .into(imageView)
    }
}