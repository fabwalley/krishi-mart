package com.fabwalley.krishimart.webservices.responsebean

import java.io.Serializable

/**
 * Created By Rahul Kansara (Rk) \n Email : rahul.kansara10@gmail.com on 09-Aug-20.
 */

data class BrandListResponse(
    val brandidlist: ArrayList<HomeResponse.Popular>
) : Serializable
