package com.fabwalley.krishimart.activity

import android.app.Application
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.fabwalley.krishimart.R
import com.fabwalley.krishimart.adapter.BrandAdapter
import com.fabwalley.krishimart.adapter.MultiAdapter
import com.fabwalley.krishimart.adapter.PopularAdapter
import com.fabwalley.krishimart.adapter.SizeAdapter
import com.fabwalley.krishimart.utils.AppPreferences
import com.fabwalley.krishimart.webservices.responsebean.HomeResponse
import com.talli.customer.common.NavigationActivity
import com.talli.customer.listners.OnItemClickListner
import com.talli.customer.webservice.ApiClient
import kotlinx.android.synthetic.main.activity_brand.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.recyle_lay.*


class BrandActivity : NavigationActivity() {

    lateinit var data: HomeResponse
    lateinit var brandAdapter: BrandAdapter
    lateinit var adapter: MultiAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_brand)
        data = intent.getSerializableExtra("data") as HomeResponse

        headerTxt.setText(data.brand[intent.getIntExtra("which", 0)].name)

        back.setOnClickListener {
            onBackPressed()
        }

        brandAdapter = BrandAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {
                val data = obj as HomeResponse.Brand
                headerTxt.setText(data.name)
                cateRv.scrollToPosition(position)
                brandAdapter.setClicked(position);
                apicall(data.id)
            }
        })

        brandAdapter.addData(data.brand)
        brandAdapter.setClicked(intent.getIntExtra("which", 0))
        cateRv.adapter = brandAdapter
        cateRv.scrollToPosition(intent.getIntExtra("which", 0))
//        setAdapter()

        adapter = MultiAdapter(mContext, object : OnItemClickListner {
            override fun onItemClick(obj: Any, position: Int, task: Int) {

            }
        })

//        recyleview.layoutManager = GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false)
        recyleview.adapter = adapter
        apicall(data.brand[intent.getIntExtra("which", 0)].id)

        filter.setOnClickListener {
            openFilter();
        }

    }

    private fun setAdapter() {
        if (AppPreferences.isVertical){
//            adapter.
        }else{

        }

    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun openFilter() {
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var layout: View? = null
        layout = inflater.inflate(R.layout.filter_layout, null)

        layout.findViewById<CrystalRangeSeekbar>(R.id.seekbar_price)
            .setOnRangeSeekbarChangeListener(OnRangeSeekbarChangeListener { minValue, maxValue ->
                layout.findViewById<TextView>(R.id.priceStart).setText("₹$minValue")
                layout.findViewById<TextView>(R.id.priceEnd).setText("₹$maxValue")
            })
        val sizelist =
            arrayOf("200gm", "500gm", "1kg", "2kg", "5kg")
        val adapter = SizeAdapter(this, sizelist)

        layout.findViewById<RecyclerView>(R.id.rvSize).adapter = adapter

        val window = PopupWindow(
            layout,
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT,
            true
        )
        window.animationStyle = R.style.slide_animation
        if (layout != null) {
            window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            window.isOutsideTouchable = true
            val displayMetrics = DisplayMetrics()
            windowManager.defaultDisplay.getMetrics(displayMetrics)
            val height = displayMetrics.heightPixels
            val width = displayMetrics.widthPixels
            window.showAtLocation(layout, Gravity.TOP, width, 0)
        }
        layout.findViewById<Button>(R.id.closeBtn)
            .setOnClickListener(View.OnClickListener { window.dismiss() })
    }

    private fun apicall(id: Int) {
        callApiAndShowDialog(call = {
            ApiClient.getClient(applicationContext as Application)
                .getByBrandId("singleproductbrandapi", id)
        }, handleSuccess = {
            adapter.addData(it.brandidlist)
            if (it.brandidlist.size == 0) {
                nodata.visibility = View.VISIBLE
            } else {
                nodata.visibility = View.GONE
            }
        }, handleGenric = {
            makeToast(it)
        }
        )
    }

    override fun onBackPressed() {
        BrandAdapter.selected = -1
        super.onBackPressed()
    }
}